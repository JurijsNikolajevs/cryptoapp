# README #

This README is for Ecenta Temenos Crypto App internal project

### How do I get set up? ###

* Prerequisites are:
*	- Updating Visualizer to the latest version available
*	- Sourcetree desktop app or CLI set up for git operations
* Configuration for Sourcetree
*	- For Login into Sourcetree and perform proper authentication go to -> Tools -> Options -> Authentications tab -> Click Add -> add HTTPS and OAuth authorization that will open BitBucket account page in browser. Authenticate to enable it in Sourcetree.
* 	- Pull latest DEVELOPMENT branch in workspace folder and refresh Visualizer to open it. Perform tests to see if changes are
* Configuration for CLI
*	- For Login into CLI select operation CLONE in branch DEVELOPMENT. Perform git clone in CLI, add credentials as requested.

### Contribution guidelines ###
*	- For each new task/subtask, create separate branch that complies with JIRA task ID - example - ICA-40 create a branch ICA-40_BUY_SELL_cryptocurrency. Branch must have description for ease of use.
*	- After successful commit and push a pull request must be made to DEVELOPMENT branch and all members added as reviewers.
*	- Before Push, it is recommended to perform Pull from the DEVELOPMENT branch to minimize code conflicts.
*	- In case there are any merge conflicts, those must be performed on lower level branch (i.e., ICA-40_BUY_SELL_cryptocurrency branch), although, this is the default behaviour.

